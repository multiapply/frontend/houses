const config = {
  webServer: {
    command: 'npm run start',
    port: 3000,
    timeout: 120 * 1000
  },
  testDir: 'tests',
  testMatch: /(.+\.)?(test|spec)\.[jt]s/
}

module.exports = config
