import { RouterProvider, createBrowserRouter } from 'react-router-dom'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import Home from './pages/Home'
import Profile from './pages/Profile'
import SignIn from './pages/SignIn'
import SignUp from './pages/SignUp'
import ForgotPassword from './pages/ForgotPassword'
import Offers from './pages/Offers'
import RootLayout from './pages/Root'
import { PrivateRoute } from './components/PrivateRoute'
import Explore from './pages/Explore'
import Category from './pages/Category'
import CreateListing from './pages/CreateListing'

const router = createBrowserRouter([
  {
    path: '/',
    element: <RootLayout />,
    children: [
      { path: '/', element: <Explore /> },
      {
        path: '/profile',
        element: < PrivateRoute/>,
        children: [
          { path: '/profile', element: <Profile/> }
        ]
      },
      { path: '/sign-in', element: <SignIn /> },
      { path: '/sign-up', element: <SignUp /> },
      { path: '/forgot-password', element: <ForgotPassword /> },
      { path: '/offers', element: <Offers /> },
      { path: '/create-listing', element: <CreateListing /> },
      { path: '/category/:categoryName', element: <Category /> }

    ]
  }
])
function App () {
  return (
    <>
      <RouterProvider router={router} />
      <ToastContainer />
    </>
  )
}

export default App
