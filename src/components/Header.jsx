import { Link, useLocation, useNavigate } from 'react-router-dom'

export default function Header () {
  const location = useLocation()
  const navigate = useNavigate()

  function pathMatchRoute (route) {
    if (route === location.pathname) {
      return true
    }
  }

  return (
    <div className='sticky top-0 z-50 | shadow-sm border-b bg-white'>
      <Link to="/profile">Profile</Link>
      <Link to="/sign-up">SignUp</Link>
      <header className='flex justify-between items-center | mx-auto px-3 max-w-6xl'>
        <figure>
          <img src="https://lahaus.imgix.net/static/branding/logo-lahaus-full.svg?auto=compress,format&dpr=2"
            alt="logo"
            className='h-5 cursor-pointer'
            onClick={() => navigate('/')}/>
        </figure>
        <nav>
          <ul className='flex'>
            <li className={`py-3 border-b-[3px] border-b-transparent text-sm font-semibold text-gray-400 ${pathMatchRoute('/') && 'text-black border-b-red-500'}`}><Link to="/">Home</Link>
            </li>
            <li className={`py-3 border-b-[3px] border-b-transparent text-sm font-semibold text-gray-400 ${pathMatchRoute('/offers') && 'text-black border-b-red-500'}`}><Link to="/offers">Offers</Link>
            </li>
            <li className={`py-3 border-b-[3px] border-b-transparent text-sm font-semibold text-gray-400 ${pathMatchRoute('/sign-in') && 'text-black border-b-red-500'} cursor-pointer`}
              onClick={() => navigate('/sign-in')}>Sign In
            </li>
          </ul>
        </nav>
      </header>
    </div>
  )
}
