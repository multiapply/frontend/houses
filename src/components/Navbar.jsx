import { useNavigate, useLocation } from 'react-router-dom'

import { ReactComponent as OfferIcon } from '../assets/svg/localOfferIcon.svg'
import { ReactComponent as ExploreIcon } from '../assets/svg/exploreIcon.svg'
import { ReactComponent as PersonOutlineIcon } from '../assets/svg/personOutlineIcon.svg'

function Navbar () {
  const location = useLocation()
  const navigate = useNavigate()

  const pathMatchRoute = (route) => {
    if (route === location.pathname) {
      return true
    }
  }
  const fillPathMatch = (route) => {
    return pathMatchRoute(route) ? '#2c2c2c' : '#8f8f8f'
  }
  const namePathMatch = (route) => {
    return pathMatchRoute(route) ? 'navbarListItemNameActive' : 'navbarListItemName'
  }

  return (
    <footer className="navbar">
      <nav className="navbarNav">
        <ul className="navbarListItems">
          <li className="navbarListItem"
            onClick={() => navigate('/')}>
            <ExploreIcon fill={fillPathMatch('/')} width='36px' height='36px' />
            <p className={namePathMatch('/')}>Explore</p>
          </li>
          <li className="navbarListItem"
            onClick={() => navigate('/offers')}>
            <OfferIcon fill={fillPathMatch('/offers')} width='36px' height='36px' />
            <p className={namePathMatch('/offers')}>Offer</p>
          </li>
          <li className="navbarListItem"
            onClick={() => navigate('/profile')}>
            <PersonOutlineIcon fill={fillPathMatch('/profile')} width='36px' height='36px' />
            <p className={namePathMatch('/profile')}>Profile</p>
          </li>
        </ul>
      </nav>
    </footer>
  )
}

export default Navbar
