import { render, screen } from '@testing-library/react'
import Home from './Home'

it('renders learn react link', () => {
  render(<Home />)
  screen.getByRole('heading', {
    name: 'Welcome to Houses'
  })
})
