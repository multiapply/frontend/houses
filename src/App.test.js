import { render, screen } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import App from './App'

it('Navigate from Home to Profile page', () => {
  render(<App />)
  screen.getByRole('heading', {
    name: 'Welcome to Houses'
  })
})
